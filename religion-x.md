---
title: Religion X
subtitle: Unser kleinster gemeinsamer Nenner sei das X; und der Wille zu überleben.
date: 16.10.2021
...

We call it: The Code of Human Conduct

# Grundstruktur

- Die Religion wird dem Linux-Ökosystem nachempfunden:
	+ open source (d.h. u.a. veränderbar)
	+ modular (d.h. rekombinierbar, erweiterbar)
	+ einheitlicher Kernel (d.h. gemeinsame fundamentale Basis)
	
# Kernel

- regelt die Grundphilosophie
- was hier nicht definiert oder eingeschränkt ist, ist automatisch frei
- Kernfunktionen:
	+ Hauptfunktion: Der Mensch muss sich sinnvoll ins Ökosystem Erde eingliedern.
		* das schließt dabei ein, dass jedem Menschen dieselben Rechte und Pflichten (aus diesem Modul) zukommen.
	+ Folgschaft klären: Der Religion gehört initial an, 1) wer sich zu ihr bekennt und 2) wer nach ihren Werten _handelt_.
		* entsprechend kann man sich für die Religion durch falches handeln auch disqualifizieren
		* umgekehrt muss sich niemand zu dieser Religion zählen lassen, nur weil er nach ihren Werten handelt; offener Konsens wird vorausgesetzt
		* entsprechend kann niemandem die Zugehörigkeit oder der Beitritt abgesprochen werden, wenn er sich an obige Regel hält
	+ Kombinierbarkeit klarstellen: Jede Modulkombination ist legal, sofern sie den Werten des überstellten Moduls nicht entgegen steht. Die freie Wahl der Module darf nie verboten werden.
		* Das schließt Glaubensfreiheit und bedingungslose akzeptanz dieser Glauben mit ein, sofern der Glauben den Werten des Kernels nicht entgegensteht
		* Ebensolches gilt für Liebesformen, Erziehungsformen und auch alles andere – macht was ihr wollt, aber a) geht damit niemanden auf den Sack und b) haltet Euch an die vorgelagerten Regeln
	+ Sinn des Lebens ist frei wählbar, muss sich jedoch der Hauptfunktion unterordnen. Idealerweise folgt der selbst gewählte Sinn des Lebens dieser Hauptfunktion (differenziert sie persönlich aus, bpsw. "Permakultur verbreiten" oder so)
		* die Wahl eines vorformulierten Lebenssinns ist ebenso möglich wie die Neuformulierung eines eigenen
		* natürlich darf der Lebenssinn auch jederzeit angepasst werden
	+ Relativismus (Nihilismus) beschreiben und wie mit 'Andersgläubigen' umgegangen werden kann (nicht: soll); das 'soll' kann in weiteren Modulen geklärt werden; hier liegt vermutlich das größte Konfliktpotential, insofern ist die Konsistenz der Logik umso wichtiger
	+ Machtstruktur der Religion klären:
		* kein Guru, keine Institution (klären, was noch legal wäre, da sich das ohnehin herausbilden wird)
		* im Namen der Religion gegen ihre Werte zu handeln ist illegal (siehe Sektion "Umgang mit Andersgläubigen")
	+ Umgang mit Macht allgemein klären
		* alles und jeder gehört ist selbst (Eigentum ist Diebstahl)
- den 4 Grundängsten ist Rechnung zu tragen, sodass alle a) hinreichend erreicht werden und b) Verständnis füreinander aufbringen, die sie andere Grundängste/andere Grundimpulse haben – bzw. anders starke. Wir müssen erkennen, dass wir alle alle diese Grundängste in uns tragen, dass diese existentielle Ängste uns einen, wir alle dieselben Grundgefahren fürchten, dass wir uns immer darauf besinnen können, eigentlich als eins gestartet zu sein und uns immer noch daran erinnern, was der andere tief in sich letztlich fühlt.
	+ Schizoider Teil
		* Wahrung der körperlich- geistigen Unversehrtheit
		* Förderung eigenen Raumes, eigener Gestaltungsmöglichkeiten
		* Wahrung eigener Entscheidungsfindung; der Autonomie und der Autarkie
		* klare, direkte Kommunikation; Wahrung der Wahrheit, echter Hintergründe; Bestrebung nach der Findung der Ursache von Dingen (siehe auch Riemann-Thomas-Modell)
	+ Depressiver Teil
		* Schutz und Unterstützung der Liebe
		* zuverlässiges Fallnetz im Moment persönlicher Krisen
		* (nahezu) bedingungslose Daseinsberechtigung
		* Vermeidung unnützer Abgrenzungen
		* Förderung menschlicher Beziehungen; Überwindung der Abgetrenntheit
	+ Zwanghafter Teil
		* genereller Bestandsschutz bewehrter Methoden
		* gemeinsame Entscheidungsfindung bei Veränderungen
		* klare Verantwortlichkeiten, unmissverständlich definierte Strukturen und Regeln
		* belastbare, neutrale Geschichtsschreibung, Beweisaufnahme etc.
	+ Hysterischer Teil
		* Förderung der Selbstwirksamkeit
		* Gnade, Verzeihung bei Fehlern/Grenzübertritten
		* grundsätzliche Freiheit in der Auslegung des Lebens, in allen Bereichen, die nicht sonstig bereits definiert sind (Glaubensfreiheit, Kunstfreiheit, Meinungsfreiheit etc.)
		* Zulassung experimenteller Felder zu Weiterentwicklung bestehender Strukturen (bspw. Medizin, Bildung, Sicherheit, Politik,…)
- Expansionsmodell der Existenzberechtigung
	+ Eine Kombination von Modulen soll sich nicht, einem Krebsgeschwür gleich, unkontrolliert ausbreiten und dabei (fast notwendigerweise) andere Modulkombinationen ausrotten. Ziel ist ein sinnvolles Ökosystem, sowohl physisch in der Welt als auch geistig in Form menschlicher Vielfalt. Eine Monokultur an Modulkombinationen wäre mit hoher Wahrscheinlichkeit instabil und auch sonst sehr arm. Gruppen, die durch Dominanz anderer Gruppen zur Monokultur werden möchten, verstoßen gegen den Kernel.
		* ergo ist dem gesamten Ökosystem, das schließt Menschen und ihre Ansichten mit ein, sinnvolles Recht und angemessener Respekt gegenüberzubringen; wer das nicht tut, verdient weder diese Rechte noch den Respekt
	
## Code of Execution

Es gelten immer die Regeln der jeweiligen Eben der Interaktion. Fallen wir in die untereste Ebene zurück, so gelten die Naturgesetze, somit auch "das Recht des stärkeren" bzw. schnelleren, listigeren etc. – es gelten die brutalen Gesetze der Evolution.

Gruppen können intern und untereinander Verhaltensregeln aushandeln. Diese können auf verschiedenen Ebenen geschlossen werden.

Zwischen den Ebenen darf gesprungen werden, denn es gelten die Gesetze der Evolution. Aus Rücksicht der Diplomatie (bzw. langfristigen Zielen) und den eigenen Werten, sollten diese Sprünge aber weise Bedacht und vor allem verhältnismäßig ausgeführt werden.

BEISPIEL Eine dem Kernel zugehörige Gruppe hat einen schweren Konflikt mit einer diesem nicht zugehörigen Gruppe (bspw. Faschisten (siehe Expansionsmodell). Beide Gruppen können diesen Konflikt in den ausgehandelten Regeln beilegen (etwa durch Herstellung gegenseitigen Verständnisses und Kompromissen). Beide Gruppen entscheiden dabei selbst, ob sie in den Ebenen der Regelsetzung springen und sich dabei potentiell das gegenseitige Vertrauen zerstören. In dieser Auseinandersetzung ließe sich also in den Ebenen sprunghaft absteigen, so käme man bspw. zu Verhandlungen, Manipulation, Erpressung oder im Extrem auch Auslöschung durch Krieg. Diese Methoden sind nicht nur extern (in dem Verhältnis zu Faschisten oder bspw. der restlichen physischen Umwelt oder der Geschichtsschreibung), sondern auch intern (bspw. Werterepräsentation innerhalb der eigenen Gruppe) mit Kosten/Konsequenzen verbunden. Da im Endeffekt die Gesetze der Evolution gelten, ist strategisch entsprechend zu Entscheiden, was _im Sinne der eigenen Gruppe_ die beste Vorgehensweise ist. Die eigenen Gesetze des Zusammenlebens z.B. aus Empathie auf andere Gruppen (etwa Faschisten) anzulegen sei zwar nobel, aber a) unberechtigt, sofern diese nicht mit jener Gruppe explizit ausgehandelt wurden und b) gefährlich, da sich die andere Gruppe diesen Regeln berechtigterweise nicht verpflichtet fühlt.

## Macht

Macht ist potentielle Energie.

Diese kann 'als Werkzeug' eingesetzt werden.
	
# Liebe

- Liebe ist grundsätzlich frei.
	- Zu lieben und in welcher Form, steht jedem frei.
	- Liebe ist nicht auf 2 Personen beschränkt
- Liebe dient dem Wohlergehen der Liebenden.
	+ das _kann_ u.a. Sicherheit bedeuten
- Niemandem steht das Recht zu, Liebe anderer zu beschränken.
- Voraussetzung für Liebe ist Konsens
- Beziehungen sind verhandelbar.
	- Grundlage für Verhandlungen ist die Fähigkeit und die Möglichkeit dazu.
		+ Somit seien bspw. patriarchale Beziehungen oder pädophile Beziehungen unzulässig.
		+ Jedoch muss hier noch eine Grenze definiert werden, worin der minimal nötige Grad von "Fähigikeit" und "Möglichkeit" bestehen müssten.

# Gott

- jeder eigene Gott ist ok, ergo
	- jeder Gott der anderen ist auch ok
	- Gott kann gewechselt werden
- Unsere Konzepte von Gott sind allesamt Metaphern. Man kann Gott (derzeit) nicht mit Konzepten einfangen. Wir sollten also nie vergessen, dass wir ggf. alle an dasselbe Gottesbild, aber mit unterschiedlichen Methoden glauben.
- Gottesglaube dient dem Wohlergehen des Gläubigen

# Kindeserziehung

- Kindeserziehung dient dem langfristigen Wohl des Kindes
	+ langfristig sei hier auf einen Zeitraum beschränkt, bis den das Kind noch nicht selbst über sein Leben entscheiden kann. Natürlich ist dieser Zeitraum fließend und sehr individuell, aber bspw. nicht 30 Jahre lang