---
title: Machtbuch
...

# Herleitung

## Der Nullpunkt

Die Natur kennt keine übergeordnete Moral. Neben den physikalischen Gesetzen unserer Welt, gilt einzig das Recht des Stärkeren zusätzlich als Naturgesetz.

Leben ist unvermeidlich damit verbunden, das irgendwas (oder -wer) anderes sterben muss. Wir umgeben uns mit totem Material (Stein, gebeiztes Buchenholz) und töten, um zu essen, um zu leben. Das Recht des Stärkeren ist der Minimalkonsens zwischen den Dingen. Wer agiler, kräftiger, klüger oder vorbereiteter ist, hat eine Machtposition inne, die er gegen andere, und seien es nur Steine und Bäume, ausnutzen kann. Zumindest kurzfristig.

Dieses Recht steht allen Elementen unserer Welt unweigerlich zu. Wesen formen Gruppen, um sich gegen andere Wesen und Gruppen besser behaupten zu können. Alle müssen fressen, niemand möchte gefressen werden.[^vega] Diesem Grundkonflikt entgeht auch eine humanistische Gesellschaft nicht. Jedes Wesen entscheidet selbst, wie es sich zu anderen Wesen stellen möchte, welchen Gruppen es wie sehr zugehören möchte; wen es fressen und zerstören möchte und wen lieben und achten. In einer Gruppe kann dies zum (weichen) Konsent ausgehandelt werden.[^fuzcon]

[^vega]: Auch nicht die Pflanzen, und vielleicht möchte nicht einmal das Wasser getrunken werden.
[^fuzcon]: #fuzzyConsent

## Der Konsent zwischen den Welten

1. Am Verhandlungstisch sitzt nur, wer kommt.[^vertretung]
2. Niemand hat, aus der Perspektive der Natur, Recht. Auch kein Recht _auf_ irgendetwas.

Gruppen handeln Regeln aus, und die Konsequenz bei deren Verstoß. Das passiert intern und gern auch zwischen Gruppen.
Das Einhalten, das Verstoßen und Andersleben, bewusst oder unbewusst, ist der Natur herzlich egal. Niemand braucht sich also an eine übergeordnete Instanz oder Moral zu richten, an die er die Verantwortung abtreten kann. In dieser Welt richtet jeder selbst.[^vertrauen]
Kurz: Es kann alles frei und mit jedem verhandelt werden[^nazis]. Machtgefälle ist hierbei nicht selten, sollte aber im Sinne des Humanismus vermieden/verringert werden. Insofern ist freie, offene, direkte/unmissverständliche Kommunikation eine Grundvoraussetzung.

[^vertretung]: …oder vertreten wird.
[^vertrauen]: …oder vertraut anderen, dass sie in einem für ihn vertretbaren Rahmen richten.
[^nazis]: Auch mit Nazis.

--

## Geteilte Macht potenziert sich

Macht, ähnlich wie Werkzeuge, die Macht ermöglichen sollen (Geld, mediale Reichweite etc.), ist selbst nur ein Werkzeug (wie alle anderen Instanzen auch in dieser Kette, bis man zu den Bedürfnissen gekommen ist).
Die meisten Machtkonzepte haben zum Kern, die Macht in einer Person zu bündeln - und ggf in eine kontrollierbare Gruppe reineskalieren zu lassen.
Das Ziel meines Machtkonzeptes ist es, Macht unabhängig von Einzelpersonen zu organisieren - und so eben den Zweck der Macht direkt zu fördern. Das, was ich durch Macht erst erreichen möchte, kann ich auch direkt fördern indem ich andere mit Macht ausstatte. Doch nicht zu viel, Machtkonzentrationen sind giftig. Allerdings, anders als Strohmänner, muss den Bemächtigten ein Handlungsspielraum zur Verfügung stehen. Die Richtung muss stimmen, die Maßnahmen und Methoden sind zweitrangig (aber keineswegs egal).
Die Ziele sind also möglichst allgemein zu halten, um so eine möglichst breites Handlungsspektrum zu erhalten. Die Menschen kämpfen, über Epochen und teils Ideologien hinweg, für ausgewählte Ziele und Ideale. Die Strömung lebt, wenn ich schon lange tot bin. Und auch ich habe sie nicht erfunden. Everything is a remix ([ref](https://youtu.be/nJPERZDfyWc)).

---

# Die einfachen Wahrheiten

Beobachtete Regeln des menschlichen Zusammenlebens, simplistisch dargestellt, sodass sie möglichst viele verstehen können:

## Eigennutz kommt vor Gemeinnutz.

Werden die Ressourcen knapp, richten sich Denken & Handeln zunehmend nach der Absicherung eines kleiner werdenden Kreises aus (Freunde, Familie, sich selbst). Man trennt sich zunehmend von den außen liegenden sozialen Schichten (der übergeordneten Gemeinschaft/Gesellschaft etwa) und damit auch von den damit einhergehenden gesellschaftlichen Werten.\
Je besser die Gemeinschaft das Wohlbefinden der einzelnen Mitglieder sichert, desto eher setzen sich diese für das Überleben derselben ein.

## Kommunikation ist alles.

Offene, ehrliche, reflektierte Kommunikation, mit sich und anderen, ist Voraussetzung für alles folgende.
Je weniger doppeldeutig oder gar -bödig und dagegen verständlicher die Kommunikation (bspw. der Gesetze an die Menschen) abläuft, desto besser kann ehrlich verhandelt werden, desto mehr können bspw. Werte mit den Individuen resonieren, desto eher tragen diese Entscheidungen von höheren, anonymen Ebenen mit, ohne diese komplett verstehen zu müssen.

## Vertrauen ist die Basis.

Nicht Verständnis und auch nicht Kontrolle. Beides hilft, aber beides verhindert eine Skallierung. Kein Mensch kann alles, nicht mal in seinem eigenen Leben, verstehen oder kontrollieren. Auch keine Gruppe. Das betrifft auch die Informationen, die ein Mensch konsumiert: Am Ende ist es das Vertrauen in die Nachrichtenquelle, in die Gruppe, der er angehört, die über Akzeptanz und Identifizierung entscheidet. Ein Mensch nimmt auch die merkwürdigsten und zerstörerischsten Werte an, wenn er nur der Gemeinschaft genügend vertraut. Und umgekehrt lässt er auch die nobelsten Werte und logischsten Zusammenhänge fallen, wenn das Misstrauen nur genügend groß ist.

## Vermeide die Konzentration von Macht.

1. Machtkonzentration ist giftig für Gemeinschaften und Individuen.
2. Macht hat vielfältige Ausprägungen. Eine Konzentration von Werkzeugen, die Macht fördert, ist ähnlich giftig und gefährlich wie die Machtkonzentration selbst.
3. Menschen sind dynamisch. Wenn jemand zu einem Zeitpunkt geeignet für eine Machtposition erschien, ist es unwahrscheinlich, dass dies ewig der Fall bleiben wird. 
  
## Be human, my friend.

Vergesse nicht Deine eigene menschliche Unzuverlässigkeit, Deine Ambivalenz. Zeige Dein Menschsein offen, sei ehrlich. Natürlich entbindet das nicht von Rechten und Pflichten. Vernachlässige in Deiner Planung einfach nicht die menschliche Komponente Deiner Mit-Im-Plan-Steckenden. Und sorge für Spaß.

# Entwurf gesellschaftlicher Grundregeln

## Liebe ist für alle da.

- Liebe ist in allen Formen grundsätzlich zu bejahen. Dem System steht kein Recht zu, Liebe zu beschränken.
- Die Menschenrechte gelten als Grundlage für diese Gesellschaftsform.
- Verteidigungskriege sollten toleriert werden, auch wenn dabei die eigenen Werte nicht gegenüber der angreifenden Partei eingehalten werden. Externe Dominanz erzeugt keinen Anspruch auf die bessere Gesellschaft. Ihr oder wir? (Ernstfall) Na, wir!
- Angriffs- und Bürgerkriege führen zur sofortigen Aushebelung unserer Werte. Zur Verurteilung braucht es keinen übergeordneten Rechtsmechanismus: Die Unmenschlichkeit ist dem Kriege inheränt.
- Regelverstöße (nach unseren Maßstäben) in anderen Gruppen (bspw. Ländern) werden wir nicht verhindern müssen; aber wir werden sie mindestens nicht (direkt) fördern (durch Waffenexporte etwa).

## Eigentum ist Diebstahl.

- Alles gehört grundsätzlich sich selbst (physisches) oder niemandem (immaterielles). Stirbt/verschwindet das Element, verlischt das Eigentum für immer.
- Eigentum ist nicht übertragbar. Die Aneignung weiteren Eigentums ist als Akt der Gewalt zu werten. Eigentum an fremden physischen Dingen (u.a. Lebewesen) ist eine Form der Leibeigenschaft, der Versklavung. Eigentum an immateriellen Dingen (u.a. Energie, physikalischen Gesetzen, Ideen, Konzepten) ist lächerlich.
- Besitz, insbesondere Erstbesitz, ist zu regeln.
  - Besitz muss an einen Zweck gebunden sein; wird dieser nicht erfüllt oder kann nicht (weiter) gewährleistet werden, verlischt der Besitzanspruch automatisch.
- Der Problematik "Der Übermensch kommt nicht mit einer Übervernunft" ist besonderer Beachtung zu schenken. Wir können mehr, als wir "natürlicherweise" sollten, als wir überschauen und intuitiv kontrollieren können.

## With great power comes great responsibility.

Geld, Macht, Einfluss, Landminen, Panzer und Handfeuerwaffen sind …Waffen.
Besitz und Gebrauch sind an Regeln gebunden.
Unnötiger Besitz von Waffen ist zu vermeiden.
Und vorzubeugen.
Unachtsamer Gebrauch wird sanktioniert und führt im Zweifel zum Entzug.
Unfälle dürfen toleriert werden. Wir sind Menschen.

## Macht korrumpiert.

Machtkonzentration führt zu einem essentiellen Ungleichgewicht, führt zu Abgrenzung, führt zu Fremdheit. Und das wiederum ist eine Grundvoraussetzung für inhumane Maßnahmen wie Krieg, Ausbeutung, Intrigen. Mächtige sind einsam und werden von machtlosen oft nicht verstanden. Das macht sie anfällig für Korruption.

Niemandem (weder Einzelpersonen noch Gruppen, Organisationen, …) steht ein Übermaß an Macht zu. Kein Gesetz darf das Gegenteil behaupten. Das gilt auch für die Werkzeuge, die Macht ermöglichen.

## Werte sind immer die Werte der Zeit.

Zeiten ändern sich. Das System muss mitgehen und braucht geeignete Korrekturmechanismen.

## Religion is personal.

- Der Grundsatz für praktische Religionsfreiheit.
- Sollte der Glauben einzelnen Werten der Gesellschaft widersprechen, kann dies toleriert werden. Dies ist allerdings regelmäßig auszuhandeln.
- Missionierung widerspricht dieser Regel, kann aber geduldet werden. Expansion scheint den Gruppen ein natürliches Bedürfnis zu sein. Sie ist jedoch mindestens zu beschränken. Dies ist regelmäßig auszuhandeln.

## Kreislaufverpflichtung

- Jedes Glied eines (Stoff-)kreislaufes baut auf dem jeweils vorangehenden Glied auf ist dem jeweils nächsten Glied verpflichtet. Der Holzfäller muss nicht unbedingt den nächsten Baum pflanzen und ihn bis zum Erntealter pflegen (wenngleich das im Sinne des menschlichen Optimierungsproblems eine wünschenswerte Konstellation wäre), aber die Aufgaben im Kreislauf müssen durchgeführt werden und die Glieder der Kette verlässlich sein.

## Austausch erhält den Frieden

Gruppen haben füreinander solange kein Verständnis, bis sie die jeweils anderen Umstände und Werte einmal kennengelernt haben (durch Rollentausch, gemeinsame Freunde etc.). Für den Frieden unter den Menschen und zwischen Mensch und Natur ist der Austausch zwischen den Welten (und bestenfalls die Abhängigkeit voneinander) essentiell.

# Entwurf politischer Handlungsempfehlungen

- Expandiere durch Liebe, festige durch Macht 
- Begrenze Macht nicht durch Gesetze (workarounds sind die logische Folge des Optimierungsproblems). Nutze indirekte Mechanismen (etwa open source). Je mehr Ebenen darin liegen, desto einfacher wird der Mechanismus akzeptiert. Direkte Gesetze hingegen werden angegriffen und umgangen
- Motivation: langfristige Erfolge geben die Vision und die Sicherheit einer Werte- und Gemeinschaftsstabilität. Kurzfristige geben die Lust.

